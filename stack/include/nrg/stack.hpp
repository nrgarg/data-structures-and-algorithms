#ifndef NRG_STACK_HPP
#define NRG_STACK_HPP
#include <cstddef>

namespace nrg {

    template <typename T>
    struct stack_node {
        T data;
        stack_node<T>* next;
        stack_node() : next(nullptr)
        {}
        stack_node(const T& data) : data(data), next(nullptr)
        {}
        stack_node(const T& data, stack_node<T>* next) : data(data), next(next)
        {}

        ~stack_node()
        {
            next = nullptr;
        }
    };

    template <typename T>
    struct stack_node_ptr {
        stack_node<T> *ptr;

        stack_node_ptr<T>(stack_node<T>* ptr) : ptr(ptr)
        {}

        ~stack_node_ptr<T>()
        {
            delete ptr;
        }
    };

    template <typename T>
    struct stack {
        std::size_t size_;
        stack_node<T>* head;

        stack() : size_(0), head(nullptr)
        {}

        std::size_t push(const T& data)
        {
            head = new stack_node<T>(data, head);
            return ++size_;
        }

        template <typename... Args>
        std::size_t emplace(Args&&... a)
        {
            return push(T(a...));
        }

        void pop()
        {
            stack_node_ptr<T> tmp(head);
            head = head->next;
            --size_;
        }

        T top()
        {
            return head->data;
        }

        std::size_t size()
        {
            return size_;
        }

        bool empty() {
            return size_ == 0;
        }

        ~stack()
        {
            while (head != nullptr) {
                stack_node_ptr<T> tmp(head);
                head = head->next;
            }
        }
    };

}
#endif
