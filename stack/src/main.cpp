#include "nrg/stack.hpp"
#include <iostream>

int main(const int argc, const char* argv[]) {
    using p = std::pair<int, int>;
    nrg::stack<int> s;
    nrg::stack<p> t;

    std::cout << "Testing basic integer stack " << '\n';
    s.push(20);
    s.push(30);
    s.push(40);
    s.push(40);
    s.push(20);
    s.push(30);
    s.emplace(40);

    while (!s.empty()) {
        auto f = s.top();
        s.pop();
        std::cout << "stack value " << f << '\n';
    }


    std::cout << s.size() << '\n';

    std::cout << "Testing pair integer stack " << '\n';
    t.emplace(20, 40);
    t.emplace(30, 30);
    t.emplace(40, 10);
    t.emplace(40, 30);
    t.emplace(20, 10);
    t.push(p { 2, 4 });
    t.emplace(40, 30);

    while (!t.empty()) {
        auto f = t.top();
        t.pop();
        std::cout << "stack value " << f.first << ',' << f.second << '\n';
    }


    std::cout << t.size() << '\n';

    std::cout << "Testing destructor does not throw\n";
    nrg::stack<int> d;
    d.push(20);
    d.push(30);
    d.push(40);
    d.push(40);
    d.push(20);
    d.push(30);
    d.emplace(40);

    std::cout << "Size: Expected 7, Actual " << d.size() << '\n';
}
