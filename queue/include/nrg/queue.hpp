#ifndef NRG_QUEUE_HPP
#define NRG_QUEUE_HPP
#include <cstddef>

namespace nrg {
    template <typename T>
    struct queue_node {
        T data;
        queue_node<T>* prev;
        queue_node<T>* next;

        queue_node() : next(nullptr)
        {}

        queue_node(const T& data) : data(data), prev(nullptr), next(nullptr)
        {}

        queue_node(const T& data, queue_node<T>* n) : data(data), prev(n), next(nullptr)
        {
            if (n != nullptr) {
                n->next = this;
            }
        }

        ~queue_node()
        {
            prev = nullptr;
            next = nullptr;
        }
    };

    template <typename T>
    struct queue_node_ptr {
        queue_node<T>* ptr;
        queue_node_ptr (queue_node<T>* ptr) : ptr(ptr)
        {}

        ~queue_node_ptr()
        {
            delete ptr;
        }
    };

    template <typename T>
    struct queue {

    private:
        queue_node<T> *head, *tail;
        std::size_t size_;

    public:
        queue() : head(nullptr), tail(head), size_(0)
        {}

        std::size_t size()
        {
            return size_;
        }

        bool empty()
        {
            return head == nullptr;
        }

        std::size_t push_back(const T& data)
        {
            tail = new queue_node<T>(data, tail);
            if (head == nullptr)
                head = tail;
            return ++size_;
        }

        template <typename... Args>
        std::size_t emplace_back(Args&&... a)
        {
            return push_back(T(a...));
        }

        void pop_front()
        {
            queue_node_ptr<T> tmp(head);
            head = head->next;
            --size_;
        }

        T front()
        {
            return head->data;
        };

        ~queue()
        {
            while (head != nullptr) {
                queue_node_ptr<T> tmp(head);
                head = head->next;
            }
        }
    };
}
#endif
