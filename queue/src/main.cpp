#include <iostream>
#include "nrg/queue.hpp"

int main(const int argc, const char* argv[])
{
    nrg::queue<int> q;
    q.push_back(20);
    q.push_back(30);
    q.push_back(40);
    q.push_back(40);
    q.push_back(20);
    q.push_back(30);
    q.emplace_back(40);

    std::cout << "Testing: Pop + front on simple integer queue\n";
    while (!q.empty()) {
        std::cout << q.front() << '\n';
        q.pop_front();
    }

    std::cout << "Size: Expected 0, Actual " << q.size() << '\n';

    nrg::queue<std::pair<int, int>> pq;
    pq.push_back({ 20, 10 });
    pq.push_back({ 30, 30 });
    pq.push_back({ 40, 20 });
    pq.emplace_back(40, 40);
    pq.push_back({ 20, 60 });
    pq.push_back({ 30, 10 });
    pq.emplace_back(40, 30);

    std::cout << "Testing: Pop + front on pair integer queue\n";
    while (!pq.empty()) {
        auto f = pq.front();
        std::cout << "[" << f.first << ", " << f.second << "]\n";
        pq.pop_front();
    }

    std::cout << "Size: Expected 0, Actual " << pq.size() << '\n';


    std::cout << "Testing: queue destructor throws no errors\n";
    nrg::queue<int> dq;
    dq.push_back(10);
    dq.push_back(20);
    dq.push_back(30);
    dq.push_back(40);

    std::cout << "Size: Expected 4, Actual " << dq.size() << '\n';
}
