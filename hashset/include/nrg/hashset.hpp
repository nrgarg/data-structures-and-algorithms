#ifndef NRG_HASHSET_HPP
#define NRG_HASHSET_HPP

#include <algorithm>
#include <functional>
#include <iostream>

namespace nrg {

    template <typename T>
    struct hashset_node {
        T key;
        bool active;
        hashset_node(const T& k) : key(k), active(true)
        {}

        hashset_node() : active(false)
        {}

        hashset_node(const hashset_node<T>& cp) : key(cp.key), active(cp.active)
        {}

        hashset_node(hashset_node<T>&& mv) : key(std::move(mv.key)), active(std::move(mv.active))
        {}


        hashset_node<T>& operator=(const hashset_node<T>& src)
        {
            hashset_node<T> tmp(src);
            operator=(std::move(tmp));
            return *this;
        }

        hashset_node<T>& operator=(hashset_node<T>&& src)
        {
            key = std::move(src.key);
            active = std::move(src.active);
            return *this;
        }

        ~hashset_node()
        {}
    };

    template <typename T>
    struct hashset_table_ptr {
        hashset_node<T> *ptr;
        hashset_table_ptr(hashset_node<T>* p) : ptr(p)
        {}

        ~hashset_table_ptr()
        {
            delete[] ptr;
        }
    };

    template <typename T>
    struct hashset_iterator {
        hashset_node<T> *current, *finish;
        hashset_iterator(hashset_node<T> *c, hashset_node<T> *f) : current(c), finish(f)
        {}

        T operator * () const {
            return current->key;
        }

        void operator ++ ()
        {
            ++current;
            while (current != finish && !current->active)
                ++current;
        }

        bool operator == (const hashset_iterator<T>& i) const {
            return current == i.current;
        }

        bool operator != (const hashset_iterator<T>& i) const {
            return current != i.current;
        }
    };

    template <typename T>
    struct hashset {
        std::size_t table_factor;
        std::size_t table_size;
        std::size_t resize_value;
        std::size_t mask;
        std::size_t used;
        hashset_node<T> *table_;

        using iterator = hashset_iterator<T>;

        hashset() : table_factor(2), table_size(2 << table_factor),
                    resize_value((table_size >> 1) + (table_size >> 3)),
                    mask(table_size - 1), used(0),
                    table_(new hashset_node<T>[table_size])
        {}

        hashset(const hashset& cp) : table_factor(cp.table_factor),
                                     table_size(cp.table_size), mask(cp.mak),
                                     used(cp.used),
                                     table_(new hashset_node<T>[table_size])
        {
            for (std::size_t i = 0; i < table_size; ++i) {
                if (cp.table_[i].active)
                    table_[i] = cp.table_[i];
            }
        }

        hashset(hashset&& mv) : table_factor(std::move(mv.table_factor)),
                                table_size(std::move(mv.table_size)),
                                mask(std::move(mv.mask)),
                                used(std::move(mv.used)),
                                table_(std::move(mv.table_))
        {
            mv.table_ = nullptr;
        }

        hashset& operator=(const hashset& cp)
        {
            hashset tmp(cp);
            operator=(std::move(tmp));

            return *this;
        }

        hashset& operator=(hashset&& mv)
        {
            table_size = std::move(mv.table_size);
            table_factor = std::move(mv.table_factor);
            resize_value = std::move(mv.resize_value);
            mask = std::move(mv.mask);
            used = std::move(mv.used);
            table_ = std::move(mv.table_);
            mv.table_ = nullptr;

            return *this;
        }

        bool insert(const T& obj)
        {
            auto data = hash_value(obj, table_);
            if (data.second) {
                table_[data.first].key = obj;
                table_[data.first].active = true;
                ++used;
                if (resizable()) {
                    resize();
                }
            }
            return data.second;
        }

        void erase(const T& obj)
        {
            auto data = hash_value(obj, table_);
            if (!data.second) {
                table_[data.first].active = false;
            }
        }

        void erase(const iterator& i)
        {
            if (i.current != nullptr)
                i.current->active = false;
        }

        void erase(const iterator& from, const iterator& to)
        {
            auto cur = from.current;
            while (cur != to.current) {
                cur->active = false;
                ++cur;
            }
        }

        iterator find(const T& obj)
        {
            auto data = hash_value(obj, table_);
            if (!data.second && table_[data.first].active) {
                return iterator(table_ + data.first, table_ + table_size);
            } else {
                return end();
            }
        }

        iterator begin() const {
            std::size_t i = 0;
            for (; table_ + i != table_ + table_size && !table_[i].active; ++i);
            return iterator(table_ + i, table_ + table_size);
        }

        iterator end() const {
            return iterator(table_ + table_size, table_ + table_size);
        }

    private:
        std::hash<T> hashcode;
        std::equal_to<T> key_eq;

        std::pair<std::size_t,bool> hash_value(const T& obj, hashset_node<T>* tbl)
        {
            using ret_t = std::pair<std::size_t, bool>;
            auto code = hashcode(obj);
            auto hashed = code & mask;
            auto skip = mask - (code & ((mask >> 1) + 1));
            while (tbl[hashed].active) {
                if (key_eq(tbl[hashed].key, obj)) {
                    return ret_t { hashed, false };
                }
                hashed = (skip + hashed) & mask;
            }
            return ret_t { hashed, true };
        }

        void swap(hashset_node<T>& n1, hashset_node<T>& n2)
        {
            hashset_node<T> tmp(n1);
            n1 = std::move(n2);
            n2 = std::move(tmp);
        }

        bool insert(hashset_node<T>& node, hashset_node<T>* tbl)
        {
            auto data = hash_value(node.key, tbl);
            swap(tbl[data.first], node);
            return true;
        }

        bool resizable() {
            return used > 0 && (resize_value < used);
        }

        void resize()
        {
            auto old_ts = table_size;
            auto old_table = table_;
            hashset_table_ptr<T> tmp(old_table);

            ++table_factor;
            table_size <<= 1;
            mask = table_size - 1;
            resize_value = (table_size >> 1) + (table_size >> 3);
            table_ = new hashset_node<T>[table_size];

            for (std::size_t i = 0; i < old_ts; ++i) {
                if (old_table[i].active) {
                    insert(old_table[i], table_);
                }
            }
        }

    public:
        ~hashset()
        {
            hashset_table_ptr<T> tmp(table_);
        }
    };

}
#endif
