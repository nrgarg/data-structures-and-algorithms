#include <iostream>
#include "nrg/hashset.hpp"

int main(const int argc, const char* argv[])
{
    nrg::hashset<std::string> hs;

    std::string i;
    while (std::cin >> i) {
        hs.insert(i);
    }

    for (auto const& h : hs) {
        std::cout << h << ',';
    }
    std::cout << '\n';

    return 0;
}
