#include <iostream>
#include "graph.hpp"

int main(const int argc, const char* argv[])
{
    graph<std::string> g;

    unsigned edges;
    std::cin >> edges;

    for (unsigned i = 0; i < edges; ++i) {
        std::string from, to;
        std::cin >> from >> to;
        g.connect(from, to);
    }

    auto const& r = g.toposort();

    for (auto const& i : r) {
        std::cout << i << ' ';
    }

    std::cout << '\n';


    return 0;
}
