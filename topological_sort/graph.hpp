#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <map>
#include <set>
#include <vector>

template <typename T>
struct graph {
    graph() = default;

    void connect(const T& from, const T& to)
    {
        edges_[to].insert(from);
    }

    std::vector<T> toposort()
    {
        std::vector<T> result;
        std::set<T> visited;
        
        for (auto const& i : edges_) {
            if (visited.find(i.first) == visited.end()) {
                dfs(visited, result, i.first);
            }
        }

        return result;
    }

    void dfs(std::set<T>& vis, std::vector<T>& res, const T& cur)
    {
        vis.insert(cur);
        for (auto const& i : edges_[cur]) {
            if (vis.find(i) == vis.end()) {
                dfs(vis, res, i);
            }
        }

        res.emplace_back(cur);
    }

private:
    std::map<T, std::set<T>> edges_;
};
#endif
