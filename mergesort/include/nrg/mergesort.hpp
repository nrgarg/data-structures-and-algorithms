#ifndef NRG_MERGESORT_HPP
#define NRG_MERGESORT_HPP

namespace nrg {
    template <typename Container>
    void merge(Container& dest, const Container& left, const Container& right)
    {
        auto d = dest.begin();
        auto l = left.begin();
        auto r = right.begin();
        auto lend = left.end();
        auto rend = right.end();

        while (l != lend && r != rend) {
            if (*l < *r) {
                *d = *l;
                ++l;
            } else {
                *d = *r;
                ++r;
            }
            ++d;
        }

        while (l != lend) {
            *d = *l;
            ++d;
            ++l;
        }

        while (r != rend) {
            *d = *r;
            ++d;
            ++r;
        }
    }

    template <typename Container>
    void mergesort(Container& c)
    {
        auto size = c.size();
        if (size > 1) {
            auto start = c.begin();
            auto mid = start + size / 2;
            Container sub1(start, mid);
            Container sub2(mid, c.end());
            mergesort(sub1);
            mergesort(sub2);
            merge(c, sub1, sub2);
        }
    }

}

#endif
