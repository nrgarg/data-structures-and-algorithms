#include <iostream>
#include <vector>

#include "nrg/mergesort.hpp"

int main(const int argc, const char* argv[])
{
    std::vector<int> vec1 { 1, 5 ,1, 4, 2 , 5};

    for (auto i : vec1) {
        std::cout << i << " ";
    }
    std::cout << '\n';
    nrg::mergesort<std::vector<int>>(vec1);
    for (auto i : vec1) {
        std::cout << i << " ";
    }
    std::cout << '\n';


    return 0;
}
