#ifndef HUFFMAN_HPP
#define HUFFMAN_HPP

#include <map>
#include <istream>
#include <vector>
#include <iostream>

struct huffman_node {
    std::string value;
    unsigned encoding;
    std::vector<huffman_node> next;

    huffman_node(const std::string& v, const unsigned& e) :
        value(v), encoding(e) {}

    huffman_node(const huffman_node& hn) :
        value(hn.value), encoding(hn.encoding), next(hn.next) {}

    huffman_node& operator=(const huffman_node& hn)
    {
        huffman_node tmp(hn);
        operator=(std::move(tmp));

        return *this;
    }

    huffman_node& operator=(huffman_node&& hn)
    {
        value = std::move(hn.value);
        next = std::move(hn.next);
        encoding = std::move(hn.encoding);
        return *this;
    }


    huffman_node(huffman_node&& hn) :
        value(std::move(hn.value)), encoding(std::move(hn.encoding)), next(std::move(hn.next)) {}

    huffman_node(const std::string& v, const unsigned& e, const huffman_node& left, const huffman_node& right) :
        value(v), encoding(e),  next(std::vector<huffman_node>({ left, right })) {}

    bool operator < (const huffman_node& rhs) const {
        return encoding > rhs.encoding;
    }

    void traverse(std::string s)
    {
        if (next.empty()) {
            std::cout << value << " " << s << '\n';
        } else {
            next[0].traverse(s + "0");
            next[1].traverse(s + "1");
        }
    }
};

struct huffman {
    std::map<std::string, unsigned> histogram;

    huffman() = default;

    void generate();
};

#endif
