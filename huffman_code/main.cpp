#include <iostream>
#include "huffman.hpp"

int main(const int argc, const char* argv[])
{
    huffman hf;
    while (std::cin.good()) {
        std::string i;
        std::cin >> i;
        ++hf.histogram[i];
    }

    hf.generate();
    return 0;
}
