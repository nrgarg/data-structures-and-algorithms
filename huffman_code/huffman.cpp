#include "huffman.hpp"
#include <vector>
#include <queue>

void huffman::generate()
{
    std::priority_queue<huffman_node> pq;
    for (auto const& i : histogram) {
        pq.emplace(i.first, i.second);
    }

    unsigned a = 0;
    while (pq.size() > 1) {
        auto left = pq.top(); pq.pop();
        auto right = pq.top(); pq.pop();

        pq.emplace("T_" + std::to_string(a++), left.encoding + right.encoding, left, right);
    }

    auto p = pq.top(); pq.pop();
    p.traverse("");
}
