#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <map>
#include <set>
#include <vector>
#include <queue>
#include <iostream>


template <typename T>
struct graph {
    graph() = default;

    void connect(const T& a, const T& b)
    {
        edges_[a].insert(b);
        edges_[b].insert(a);
    }

    std::vector<T> bfs(const T& start)
    {
        std::vector<T> result;
        std::set<T> visited;
        std::queue<T> q;

        q.emplace(start);
        visited.insert(start);

        while (!q.empty()) {
            auto const& f = q.front();
            q.pop();

            result.emplace_back(f);

            for (auto const& i : edges_[f]) {
                if (visited.find(i) == visited.end()) {
                    visited.insert(i);
                    q.emplace(i);
                }
            }
        }

        return result;
    }

private:
    std::map<T, std::set<T>> edges_;

};
#endif
