#include <iostream>
#include <algorithm>
#include <iterator>
#include "graph.hpp"


int main(const int argc, const char* argv[])
{

    graph<std::string> g;

    unsigned edges;
    std::cout << "Edges?\n";
    std::cin >> edges;
    for (unsigned i = 0; i < edges; ++i) {
        std::cout << "\nEdge " << (i + 1) << ": ";
        std::string from, to;
        std::cin >> from >> to;
        g.connect(from, to);
    }

    std::cout << "Starting search with?\n";
    std::string start;
    std::cin >> start;

    auto const& r = g.bfs(start);

    std::copy(r.begin(), r.end(), std::ostream_iterator<std::string>(std::cout, " "));
    std::cout << '\n';

    return 0;
}
