#include "server.hpp"
#include <future>

server::server(const char* ip_address, const unsigned& port)
    : ip_address(ip_address), port(port) {}

void server::stop()
{
    close(socket_desc);
}

int handler(const int& desc)
{
    int read_size;
    char client_message[2000];

    while ((read_size = recv(desc, client_message, 2000, 0) > 0)) {
        write(desc, client_message, strlen(client_message));
        memset(&client_message[0], 0, 2000);
    }

    if (read_size == 0) {
        std::cout << "Client disconnected\n";
        fflush(stdout);
    } else if (read_size == -1) {
        std::cerr << "Receive failed\n";
        return 1;
    }

    return 0;
}

void server::handle_connection(const int& desc)
{
    auto t = std::thread(handler, desc);
    t.detach();
}

void server::start()
{
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc == -1) {
        std::cerr << "Could not create socket\n";
        return;
    }

    srv.sin_addr.s_addr = inet_addr(ip_address);
    srv.sin_family = AF_INET;
    srv.sin_port = htons(port);

    if (bind(socket_desc, (struct sockaddr*)&srv, sizeof(srv)) < 0) {
        std::cerr << "Could not bind server\n";
        return;
    }

    if (listen(socket_desc, 0) < 0) {
        std::cerr << "Could not listen on port\n";
        return;
    }
    std::cout << "Started server on " << ip_address << ":" << port << '\n';

    auto c = sizeof(struct sockaddr_in);

    std::cout << "Waiting for clients\n";
    while ((connect_socket = accept(socket_desc, (struct sockaddr *)&cli, (socklen_t*)&c)) >= 0) {
        std::cout << "Connection accepted\n";
        handle_connection(connect_socket);
    }

    if (connect_socket < 0) {
        std::cerr << "Accepting client failed " << std::endl;
        return;
    }
}
