#include <iostream>
#include "server.hpp"

int main(const int argc, const char* argv[])
{
    server srv(argv[1], atoi(argv[2]));

    srv.start();

    return 0;
}
