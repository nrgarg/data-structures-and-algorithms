#ifndef SERVER_HPP
#define SERVER_HPP

#include <sys/socket.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>

#include <iostream>

struct server {

    server() = default;
    server(const char*, const unsigned&);
    void start();
    void stop();

    void handle_connection(const int&);

    ~server()
    {
        stop();
    }

private:
    int socket_desc;
    int connect_socket;
    const char* ip_address;
    unsigned port;
    struct sockaddr_in srv, cli;
};
#endif
