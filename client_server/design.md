# Client server design specification

## Overall implementation
Using named pipes, interact between multiple clients and potentially multiple servers

## Server
When a server is created i.e.

```shell
create-server one
create-server two
```

will create two servers named - _one_ and _two_.
* it should implemented such that the servers can communicate to eachother.
* clients should be able to interact with any server
* global events can be passed to all necessary parties


## Client

```shell
create-client one
> "hello from client1"
> "hello again from client1"
...

create-client two
> "hello from client2"
> "hello again from client2"
```

