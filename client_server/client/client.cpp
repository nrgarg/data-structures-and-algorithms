#include "client.hpp"
#include "cstring"

client::client(const char* ip_address, const unsigned& port)
    : ip_address(ip_address), port(port)
{}


void client::start()
{
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc == -1) {
        std::cerr << "Could not create socket\n";
        return;
    }

    server.sin_addr.s_addr = inet_addr(ip_address);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    if (connect(socket_desc, (struct sockaddr*)&server, sizeof(server)) < 0) {
        std::cerr << "Could not bind server\n";
        return;
    }

    char response[2000];
    while (std::cin.good()) {
        std::string message;
        std::getline(std::cin, message);

        if (write(socket_desc, message.c_str(), message.length()) < 0) {
            std::cout << "Failed to send message\n";
        }

        if (read(socket_desc, response, 2000) < 0) {
            std::cout << "Failed to receive reply from server\n";
        }
        std::cout << "Response: " << response << '\n';
        memset(&response[0], 0, 2000);
    }

}

void client::stop()
{
    close(socket_desc);
}

