#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <iostream>

struct client {

    client() = default;
    client(const char*, const unsigned&);

    void start();
    void stop();

    ~client()
    {
        stop();
    }

private:
    int socket_desc;
    int connect_socket;
    const char* ip_address;
    const unsigned port;
    struct sockaddr_in server;
};

#endif
