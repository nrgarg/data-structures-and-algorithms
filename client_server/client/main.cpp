#include <iostream>
#include "client.hpp"

int main(const int argc, const char* argv[])
{
    client cli(argv[1], atoi(argv[2]));

    cli.start();
    return 0;
}
